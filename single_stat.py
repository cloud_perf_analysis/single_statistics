#!/usr/bin/python3

""" Code for Cloud performance testing, single point of statistic estimates 
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import seaborn as sns

"""
load all data points from file
"""
def load_data(filename):
    all_data = []

    f = open(filename)

    for line in f:
        val = float(line.strip("\n"))
        all_data.append(val)

    f.close()

    return all_data

"""
extract data for the first week, second week, first two weeks, and the rest

all_data: the array that contains all of the data
total_weeks: the total number of weeks in all_data
week_to_extract: which week to extract, 1st, 2nd etc...

returns: data the week
"""
def extract_week(all_data, total_weeks, week_to_extract):
    
    one_week_len = len(all_data) // total_weeks

    begin = (week_to_extract - 1) * one_week_len
    end = week_to_extract * one_week_len

    data = all_data[begin:end]

    return data


"""
extract hourly samples from data

all_data: the original data to sample
hours: the number of hours in the all_data
samples_per_hour: samples per hour

return hourly sampled data, hourly data point count in all_data
"""
def hourly_sample_data(all_data, hours, samples_per_hour):
    # computer number of data points per hours
    hour_len = len(all_data) // hours
    
    # samples per hour to use
    samples_per_hour = samples_per_hour
    sample_rate = hour_len//samples_per_hour

    # extract samples
    samples = all_data[::sample_rate]

    return samples, hour_len

"""
break an array of data into blocks for moving-window block bootstrap

all_data: the original data
block_interval: length of the block in terms of hours
samples_per_hour: samples per hour in all_data

return array of the blocks
"""
def extract_mvwin_blocks(all_data, block_interval, samples_per_hour):
    # compute the number of samples per hour
    block_length = block_interval * samples_per_hour

    # break the samples into blocks
    # first handle the samples that can form full blocks
    block_count = len(all_data) - block_length

    blocks = []

    for i in range(block_count):
        block = all_data[i:i+block_length]
        blocks.append(block)

    return blocks


""" 
moving-window block bootstrap

blocks: the blocks to bootstrap (should be an numpy array)

return flattened bootstrap samples
"""
def mvwin_block_bootstrap(blocks):
    # compute the name of samples to pick for each bootstrap
    block_count = blocks.shape[0]
    block_length = blocks.shape[1]
    even_block_count = block_count//block_length
    
    # sample the indices
    indices = np.random.choice(blocks.shape[0], size=even_block_count,
                               replace=True)
    bt_samples = blocks[indices]

    return bt_samples.flatten()


"""
Bootstrap the mean/percentile1/percentile2

iters: iters to bootstrap
data: original data to bootstrap
bootstrap_func: name of the bootstrap function

return bt_samples
"""
def bootstrap(iters, data, bootstrap_func):

    bt_samples = []

    for i in range(0, iters):
        # get a bootstrap sample
        bt_sample = bootstrap_func(data)
        bt_samples.append(bt_sample)

    return bt_samples


"""
examine means and errors for means from the bootstrapped samples

bt_samples: bootstrapped samples
true_mean: the true mean for computing the errors

return bt_means, bt_mean_errs 
"""
def gen_bt_means(bt_samples, true_mean):
    bt_means = []
    bt_mean_errs = []
    
    for bt_sample in bt_samples:
        bt_mean = np.mean(bt_sample)
        bt_mean_err = (bt_mean - true_mean)/true_mean * 100

        bt_means.append(bt_mean)
        bt_mean_errs.append(bt_mean_err)

    return bt_means, bt_mean_errs


"""
examine means and errors for means from the bootstrapped samples

bt_samples: bootstrapped samples
ptile: the percentile to extract
true_ptile: the true mean for computing the errors

return bt_ptiles, bt_ptile_errs
"""
def gen_bt_ptiles(bt_samples, ptile, true_ptile):
    bt_ptiles = []
    bt_ptile_errs = []
    
    for bt_sample in bt_samples:
        bt_ptile = np.percentile(bt_sample, ptile)
        bt_ptile_err = (bt_ptile - true_ptile)/true_ptile * 100

        bt_ptiles.append(bt_ptile)
        bt_ptile_errs.append(bt_ptile_err)

    return bt_ptiles, bt_ptile_errs

"""
Compute the percentage differences for the means of two groups of samples.
samples1 are used as the base for computing the percentage differences.

samples1: bootstrapped samples1
samples2: bootstrapped samples2

return mean_diffs
"""

def gen_mean_differences(samples1, samples2):

    mean_diffs = [] 
    
    for sample1, sample2 in zip(samples1, samples2):
        mean1 = np.mean(sample1)
        mean2 = np.mean(sample2)

        diff = (mean1 - mean2)/mean1 * 100

        mean_diffs.append(diff)

    return mean_diffs

"""
Compute the percentage differences for the p-tiles of two groups of samples.
samples1 are used as the base for computing the percentage differences.

samples1: bootstrapped samples1
samples2: bootstrapped samples2

return diffs
"""

def gen_ptile_differences(samples1, samples2, ptile):

    diffs = [] 
    
    for sample1, sample2 in zip(samples1, samples2):
        p1 = np.percentile(sample1, ptile)
        p2 = np.percentile(sample2, ptile)

        diff = (p1 - p2)/p1 * 100

        diffs.append(diff)

    return diffs
